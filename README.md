LOG
20201123 Added Apple TV to hoobs

20210220 Specified rpi3 as tailscale exit node, therefore enabeld IP4 forwarding


CONTENTS

<!-- TOC -->

- [TODO:](#todo)
- [1. LOG](#1-log)
- [2. OPENHAB](#2-openhab)
  - [2.1. console](#21-console)
  - [2.2. items](#22-items)
- [3. HOOBS docker on openhabian*](#3-hoobs-docker-on-openhabian)
  - [3.1. Setup](#31-setup)
  - [3.3. Backups](#33-backups)
  - [Image Backups](#image-backups)
  - [Image Restore](#image-restore)

<!-- /TOC -->

# TODO:

<a id="markdown-todo%3A" name="todo%3A"></a>
- [ ] 1. setup raspibackup, saving on usb drive and maybe on timecapsule
- [ ] 2. automtically run dksnap and export docker container snapshot to usb
- [ ] 3. Setup subrepo for openhabian config, run git on rpi directly

# 1. LOG

<a id="markdown-log" name="log"></a>
20201109:   Fresh openhabian, [(3)][3] proofs to kill all hoobs items, if not on wifi -> removed, raspibackup installed

20201107:   added package.json from /opt/appdata/hoobs folder (volume of hoobs 3.2.10 core docker container)

20201106:   update openhabian to obenhab 2.5.1 and install hoobs docker container for homekitting.

# 2. OPENHAB

<a id="markdown-openhab" name="openhab"></a>

## 2.1. console

<a id="markdown-console" name="console"></a>

'ssh://openhab:<password>@<openhabian-IP> -p 8101'
smb for backing up config to this very repo:

smb://openhabian@192.168.1.111

## 2.2. items

<a id="markdown-items" name="items"></a>

Linking items with channels via .items file and remove lining in paper UI, if replaced by linking in time file.

# 3. HOOBS docker on openhabian*

<a id="markdown-hoobs-docker-on-openhabian*" name="hoobs-docker-on-openhabian*"></a>

## 3.1. Setup

<a id="markdown-setup" name="setup"></a>
- HOOBS running in docker hoobs/hoobs:xxxx
- volume: see docker file
- backups made from within hoobs' webinterface 192.168.1.111:80, gitted in config.json and .hbfx file
- using Remote-SSH extension for vscode to edit from Mac [(4)][4]
- package.json added for overview, this is probably not needed if hbfx file is known.

https://github.com/peteakalad/homebridge-dyson360eye proofs to kill all hoobs items, if not on wifi -> removed

1. docker
   Installation of docker and compose on rpi: [(2)][2]

## 3.3. Backups

<a id="markdown-backups" name="backups"></a>

1. NOT SET UP YET!!! openhabian runs its backups on the usb drive attached to the pi (this should at least backup the openhab part), use raspibackup
   backed up config using openhabian-config function for that, paths:

   /var/lib/openhab2/backups/openhab2-backup/xxxxxxx

via ```openhabian-config ...```

2. docker snaphots using dksnap [(1)][1]
3. Homekit rules backed by ios app "controller"

## Image Backups

```time sudo dd if="/dev/rdisk9" bs=96m  | pv -s 32G |sudo bzip2 --best > "/Users/riko//Downloads/20210203_01_rpi3_hemmerde.img.bz2```

---

## Image Restore

```
sudo time bunzip2 -c 20210203_01_rpi3_hemmerde.img.bz2 | pv -s 32G | sudo dd of=/dev/disk6s1 bs=96m  # to restore the image
```

Making References:
[Have a look at changes](#1.-log)

(1) https://dzone.com/articles/dksnap-docker-snapshots-for-development-and-test-d

(2) https://dev.to/rohansawant/installing-docker-and-docker-compose-on-the-raspberry-pi-in-5-simple-steps-3mgl

(3) https://github.com/peteakalad/homebridge-dyson360eye

(4) https://code.visualstudio.com/docs/remote/ssh

TODO:

Hoobs crashes, when configring this:

Provder list selectable by: (not installed yet)
https://developer.aliyun.com/mirror/npm/package/homebridge-appletv-now-playing/v/0.1.0

[1]: https://dzone.com/articles/dksnap-docker-snapshots-for-development-and-test-d
[2]: https://dev.to/rohansawant/installing-docker-and-docker-compose-on-the-raspberry-pi-in-5-simple-steps-3mgl
[3]: https://github.com/peteakalad/homebridge-dyson360eye
[4]: (https://code.visualstudio.com/docs/remote/ssh)
