#!/usr/bin/env bash
git clone https://github.com/kelda/dksnap
cd dksnap
GO111MODULE=on go build .
./dksnap