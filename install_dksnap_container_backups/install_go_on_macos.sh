platform='darwin-amd64'
#platform='armv6l'
#export GOLANG="$(curl https://golang.org/dl/|grep $platform|grep -v beta|head -1|awk -F\> {'print $3'}|awk -F\< {'print $1'})"
export GOLANG="$(curl https://golang.org/dl/|grep $platform | grep -v beta|head -1 | awk -F\= {'print $3'} | tr -d '\">' | awk -F\/ {'print $3'})"
url="https://golang.org/dl/"$GOLANG
wget $url

packagepath=$PWD$"/"$GOLANG
chmod +x $packagepath
installer -pkg $packagepath -target /usr/local
#sudo tar -C /usr/local -xzf $GOLANG;
rm $GOLANG;
unset GOLANG;

echo 'PATH=$PATH:/usr/local/go/bin' >> ~/.profile;
echo 'GOPATH=$HOME/golang ' >> ~/.profile;

echo 'PATH=$PATH:/usr/local/go/bin' >> ~/.zsh;
echo 'GOPATH=$HOME/golang ' >> ~/.zsh;

zsh 

bash 
zsh