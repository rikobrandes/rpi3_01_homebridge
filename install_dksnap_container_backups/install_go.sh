#platform='armv6l'
export GOLANG="$(curl https://golang.org/dl/|grep armv6l|grep -v beta|head -1|awk -F\> {'print $3'}|awk -F\< {'print $1'})"
#export GOLANG="$(curl https://golang.org/dl/|grep $platform|grep -v beta|head -1|awk -F\> {'print $3'}|awk -F\< {'print $1'})"
#export GOLANG="$(curl https://golang.org/dl/|grep $platform | grep -v beta|head -1 | awk -F\= {'print $3'} | tr -d '\">' | tr -d '\/dl/')"
url="https://golang.org/dl/"$GOLANG
wget $url
sudo tar -C /usr/local -xzf $GOLANG;

rm $GOLANG;
unset GOLANG;

echo 'PATH=$PATH:/usr/local/go/bin' >> ~/.profile;
echo 'GOPATH=$HOME/golang ' >> ~/.profile;
source ~/.profile

#echo 'PATH=$PATH:/usr/local/go/bin' >> ~/.zsh;
#echo 'GOPATH=$HOME/golang ' >> ~/.zsh;
# source ~/.zsh
