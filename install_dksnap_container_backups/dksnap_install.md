
**Contents**

<!-- TOC -->

- [1. Installation dksnap for taking docker snapshots](#1-installation-dksnap-for-taking-docker-snapshots)

<!-- /TOC -->

# 1. Installation dksnap for taking docker snapshots
<a id="markdown-installation-dksnap-for-taking-docker-snapshots" name="installation-dksnap-for-taking-docker-snapshots"></a>

1. install go

use install script [[1]]('./install.go.sh') and set executable flag before running it. 


[[1]](install_go.sh) install_go.sh

2. build dksnap for raspi

```
    git clone https://github.com/kelda/dksnap
    cd dksnap
    GO111MODULE=on go build .
    ./dksnap
```